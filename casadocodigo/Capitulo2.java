import java.util.List;
import java.util.Arrays;
import java.util.function.Consumer;

class Usuario {
  private String nome;
  private int pontos;
  private boolean moderador;

  public Usuario(String nome, int pontos) {
    this.pontos = pontos;
    this.nome = nome;
    this.moderador = false;
  }

  public String getNome() {
    return nome;
  }

  public int getPontos() {
    return pontos;
  }

  public void tornaModerador() {
    this.moderador = true;
  }

  public boolean isModerador() {
    return moderador;
  }
}

class Mostrador implements Consumer<Usuario> {
  public void accept(Usuario u) {
    System.out.println(u.getNome());
  }
}

public class Capitulo2 {
  public static void main(String ... args) {
    Usuario user1 = new Usuario("Felipe Zanardo Affonso", 100);
    Usuario user2 = new Usuario("Monique Vassoler Guerrero Affonso", 100);
    Usuario user3 = new Usuario("Nicolas Guerrero Affonso", 100);

    List<Usuario> usuarios = Arrays.asList(user1, user2, user3);

    System.out.println("Class Mostrador Prints:");
    Mostrador mostrador = new Mostrador();
    usuarios.forEach(mostrador);

    System.out.println("");

    System.out.println("Consumer<Usuario> Prints:");
    Consumer<Usuario> mostradorConsumer = new Consumer<Usuario>() {
      public void accept(Usuario u) {
        System.out.println(u.getNome());
      }
    };
    usuarios.forEach(mostradorConsumer);

    System.out.println("");

    System.out.println("Consumer<Usuario> inline Prints:");

    usuarios.forEach(new Consumer<Usuario>() {
      public void accept(Usuario u) {
        System.out.println(u.getNome());
      }
    });

    Consumer<Usuario> mostradorLambda = (Usuario u) -> {System.out.println(u.getNome());};

    Consumer<Usuario> mostradorLambdaInferindo = u -> {System.out.println(u.getNome());};

    Consumer<Usuario> mostradorLambdaInferindoUmaComando = u -> System.out.println(u.getNome());

    System.out.println("");

    System.out.println("Lambda Single Line Prints: ");
    usuarios.forEach(u -> System.out.println(u.getNome()));

    usuarios.forEach(u -> u.tornaModerador());

  }
}
